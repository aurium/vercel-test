module.exports = (req, res) => {

  res.json({
    now: new Date(),
    headers: req.headers,
    body: req.body,
    query: req.query,
    cookies: req.cookies,
  })

}
